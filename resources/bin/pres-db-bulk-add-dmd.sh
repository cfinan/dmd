#!/bin/bash
abort() {
    echo "[info] aborting..." 1>&2
    cp "$BAK_DB" "$PRES_DB"
    exit 1
}

PROG_NAME="=== pres-db-bulk-add-dmd ==="
USAGE=$(
cat <<EOF
$PROG_NAME
Bulk load all the DM+D databases into the prescription string database. The
 <pres-str-db> should be the SQLite DM+D database and the <dmd-dir> should
 be the root of the directory that contains DM+D SQLite databases. These
 should be built with dmd-build. The databases are loaded from newest to
  oldest

USAGE: $0 [flags] <pres-str-db> <dmd-dir>
EOF
)
. shflags

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Make sure the root directory is set
if [[ -z "$1" ]]; then
  echo "[error] no prescription string database defined" 2>&1
  exit 1
fi

# Make sure the root directory is set
if [[ -z "$2" ]]; then
  echo "[error] no DM+D root dir defined" 2>&1
  exit 1
fi

# exit on error and undef not allowed
set -eu

# source in some common functions
run_dir="$(dirname $0)"

PRES_DB=$(readlink -f "$1")
ROOT_DIR=$(readlink -f "$2")

echo "$PROG_NAME"
echo "[info] prescription string database: $PRES_DB"
echo "[info] DM+D root dir: $ROOT_DIR"

curdir="$PWD"

dmd_dbs=(
    $(find "$ROOT_DIR" -type f -name "nhsbsa_dmd_*.db" |
          perl -ne 'chomp $_;$_ =~ m/_(\d+).db$/; print $_." ".$1."\n"' |
          sort -k2nr,2 | awk '{print $1}')
)

if [[ ${#dmd_dbs[@]} -eq 0 ]]; then
    echo "[error] no DM+D databases found!" 2>&1
    exit 1
else
    echo "[info] found DM+D databases: ${#dmd_dbs[@]}" 2>&1
fi

trap 'abort' SIGINT ERR

for i in ${dmd_dbs[@]}; do
    indb="$(readlink -f "$i")"
    BAK_DB=PRES_DB."BAK"
    cp "$PRES_DB" "$BAK_DB"

    echo "[info] processing: $indb"
    dmd-pres-db-dmd -vv "$indb" "$PRES_DB"
done

echo "*** END ***"
