#!/usr/bin/env python
"""Throwaway metamap test script
"""
from umls_tools.metamap import metamap
import csv
import pprint as pp

METAMAP_ARGS = ['-L', '2020AA', '-Z', '2020AA', '-sAIy', '--negex',
                '--conj', '-V', 'USAbase']
# METAMAP_ARGS = ['-yT']


break_at = 2
# print(metamap.MetamapServer.find_server_processes())
# ms = metamap.MetamapServer(keep_alive=False)
# print(ms.start())
# print(ms.stop())
# print(ms.run_test_command())

with metamap.MetamapServer(keep_alive=True) as ms:
    with open("/scratch/dmd/dmd/mapper/test_no_mapping.txt", 'rt') as infile:
        reader = csv.DictReader(infile, delimiter="\t")
        idx = 0
        for row in reader:
            pp.pprint(row)
            results = ms.run(row['productname'], METAMAP_ARGS)
            pp.pprint(results)
            if idx == break_at:
                break
            idx += 1
