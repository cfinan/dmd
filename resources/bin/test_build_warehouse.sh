#!/bin/bash
set -e
# A temp script to build the warehouse database
whdb="/scratch/dmd_warehouse.db"
# rm "$whdb"
root_dir="/data/dmd/dmd_dbs"
limit=1000
versions=(
    $(
        ls "$root_dir"/*/*.db |
            awk '{file=$1; sub(/.*\/nhsbsa_dmd_[0-9]+\.[0-9]+\.[0-9]+_/, "", $1); sub(/\.db$/, "", $1); print $1, file}' |
            sort -k1n,1n |
            cut -f2 -d' '
    )
)

for i in "${versions[@]}"; do
    echo "$i"
done
# idx=1
# create=1
# for i in "${versions[@]}"; do
#     echo "$i"
#     if [[ $create -eq 0 ]]; then
#         dmd-warehouse -v add "$i" "$whdb"
#     else
#         dmd-warehouse -v add --create "$i" "$whdb"
#     fi
#     if [[ $idx -eq $limit ]]; then
#         break
#     fi
#     idx=$((idx + 1))
#     sleep 3
#     create=0
# done
# echo "*** end ***"
