#!/usr/bin/env python
# Throwaway cleanup script to producing a dictionary of homeopathic remedies
import os
import re
import csv

infile = "/data/dmd/cprd_mapping/hom_list.txt"
outfile = "/data/dmd/cprd_mapping/dmd_dict_hom_prov.txt"
HOM_REG = re.compile(r'^(?P<SUBSTANCE>[\w\s]+)\s+\d+[Cc]\s+')
HOM_REG_BAK = re.compile(r'^(?P<SUBSTANCE>[\w\s]+)\s+homeopathic')
with open(infile) as incsv:
    reader = csv.reader(incsv, delimiter="\t")
    with open(outfile, 'wt') as outcsv:
        writer = csv.writer(
            outcsv, delimiter="\t", lineterminator=os.linesep
        )
        writer.writerow(["original", "match"])
        for row in reader:
            try:
                writer.writerow([row[1], row[1]])
            except IndexError:
                hom_match = HOM_REG.match(row[0])

                try:
                    writer.writerow([row[0], hom_match.group('SUBSTANCE')])
                except AttributeError:
                    hom_match = HOM_REG_BAK.match(row[0])
                    try:
                        writer.writerow([row[0], hom_match.group('SUBSTANCE')])
                    except AttributeError:
                        writer.writerow([row[0], None])
