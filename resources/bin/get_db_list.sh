#!/bin/bash
# Outputs a list of the DM+D databases sorted on version number, lowest to
# highest. This assumes that the databases are in directories by year, and
# have a .db extension and the original base file name. so
# <ROOT>/YYYY/nhsbsa_dmd_*.db
set -e
db_root="$1"
db_root=$(readlink -f "$db_root")

# A temp script to build the warehouse database
versions=(
    $(
        ls "$db_root"/*/*.db |
            awk '{file=$1; sub(/.*\/nhsbsa_dmd_[0-9]+\.[0-9]+\.[0-9]+_/, "", $1); sub(/\.db$/, "", $1); print $1, file}' |
            sort -k1n,1n |
            cut -f2 -d' '
    )
)

for i in "${versions[@]}"; do
    echo "$i"
done
