#!/bin/bash
# A script to build a year's worth of DM+D databases
# will only build if they do not exist already
# Run this from the directory that it is in
indir="$1"
outdir="$2"

mkdir -p $outdir

if [[ ! -d $outdir ]] || [[ ! -d $indir ]]; then
	echo "can't find input and/or output directory" 1>&2
	exit 1
fi

echo $indir
echo $outdir
set -e
for i in "$indir"/nhsbsa_dmd_*.zip; do
	echo "$i"
	bonus_file=$(echo "$i" | sed 's/nhsbsa_dmd_/nhsbsa_dmdbonus_/')
	echo "$bonus_file"
	if [[ ! -e "$bonus_file" ]]; then
		echo "can't find bonus file: $bonus_file" 1>&2
		exit 1
	fi

	outdb="${outdir}"/$(basename "$i" | sed 's/.zip$/.db/')
	if [[ ! -e "$outdb" ]]; then
		echo "$outdb"
		dmd-build -v -B "$bonus_file" "$i" "$outdb"
	fi
done
