====================
Command line scripts
====================

There are several command line scripts that are installed with the package. These are detailed below

Building the DM+D database
==========================

.. _dmd_build:

``dmd-build``
-------------

.. argparse::
   :module: dmd.build
   :func: _init_cmd_args
   :prog: dmd-build

Example
~~~~~~~

To build an SQLite database (``nhsbsa_dmd_2.3.0_20220228000001.db``) from the input DM+D zip (and bonus zip) file, run this:

.. code-block:: console

		$ dmd-build -v -B nhsbsa_dmdbonus_2.3.0_20220228000001.zip nhsbsa_dmd_2.3.0_20220228000001.zip nhsbsa_dmd_2.3.0_20220228000001.db

After running the build process with ``--verbose`` you should see an output similar to this:

.. code-block:: console

   = dmd-build (dmd v0.1.0a1) =
   [info] bonus_files value: nhsbsa_dmdbonus_2.3.0_20220228000001.zip
   [info] config value: ~/.db_conn.cnf
   [info] config_section value: dmd
   [info] dburl value: nhsbsa_dmd_2.3.0_20220228000001.db
   [info] exists value: False
   [info] indata value: nhsbsa_dmd_2.3.0_20220228000001.zip
   [info] tmp value: None
   [info] verbose value: True
   [info] loading lookup.COMBINATION_PACK_IND: 100% 2/2 [00:00<00:00, 10082.46 rows/s]
   [info] loading lookup.COMBINATION_PROD_IND: 100% 2/2 [00:00<00:00, 11413.07 rows/s]
   [info] loading lookup.BASIS_OF_NAME: 100% 7/7 [00:00<00:00, 18772.46 rows/s]
   [info] loading lookup.NAMECHANGE_REASON: 100% 5/5 [00:00<00:00, 16033.27 rows/s]
   [info] loading lookup.VIRTUAL_PRODUCT_PRES_STATUS: 100% 6/6 [00:00<00:00, 16194.22 rows/s]
   [info] loading lookup.CONTROL_DRUG_CATEGORY: 100% 11/11 [00:00<00:00, 16384.00 rows/s]
   [info] loading lookup.LICENSING_AUTHORITY: 100% 5/5 [00:00<00:00, 14364.05 rows/s]
   [info] loading lookup.UNIT_OF_MEASURE: 100% 182/182 [00:00<00:00, 11933.71 rows/s]
   [info] loading lookup.FORM: 100% 192/192 [00:00<00:00, 9986.07 rows/s]
   [info] loading lookup.ONT_FORM_ROUTE: 100% 341/341 [00:00<00:00, 17161.31 rows/s]
   [info] loading lookup.ROUTE: 100% 74/74 [00:00<00:00, 7737.80 rows/s]
   [info] loading lookup.DT_PAYMENT_CATEGORY: 100% 12/12 [00:00<00:00, 18552.03 rows/s]
   [info] loading lookup.SUPPLIER: 100% 2173/2173 [00:00<00:00, 20441.89 rows/s]
   [info] loading lookup.FLAVOUR: 100% 236/236 [00:00<00:00, 15325.93 rows/s]
   [info] loading lookup.COLOUR: 100% 147/147 [00:00<00:00, 13524.67 rows/s]
   [info] loading lookup.BASIS_OF_STRNTH: 100% 2/2 [00:00<00:00, 10485.76 rows/s]
   [info] loading lookup.REIMBURSEMENT_STATUS: 100% 2/2 [00:00<00:00, 11008.67 rows/s]
   [info] loading lookup.SPEC_CONT: 100% 2/2 [00:00<00:00, 11366.68 rows/s]
   [info] loading lookup.DND: 100% 2/2 [00:00<00:00, 11290.19 rows/s]
   [info] loading lookup.VIRTUAL_PRODUCT_NON_AVAIL: 100% 2/2 [00:00<00:00, 11570.49 rows/s]
   [info] loading lookup.DISCONTINUED_IND: 100% 2/2 [00:00<00:00, 11366.68 rows/s]
   [info] loading lookup.DF_INDICATOR: 100% 3/3 [00:00<00:00, 11848.32 rows/s]
   [info] loading lookup.PRICE_BASIS: 100% 4/4 [00:00<00:00, 14513.16 rows/s]
   [info] loading lookup.LEGAL_CATEGORY: 100% 4/4 [00:00<00:00, 14376.36 rows/s]
   [info] loading lookup.AVAILABILITY_RESTRICTION: 100% 8/8 [00:00<00:00, 17287.19 rows/s]
   [info] loading lookup.LICENSING_AUTHORITY_CHANGE_REASON: 100% 8/8 [00:00<00:00, 16727.03 rows/s]
   [info] loading ingredient.INGREDIENT_SUBSTANCES: 100% 3686/3686 [00:00<00:00, 15057.33 rows/s]
   [info] loading vtm.VIRTUAL_THERAPEUTIC_MOIETIES: 100% 3028/3028 [00:00<00:00, 21873.51 rows/s]
   [info] loading vmp.VMPS: 100% 22361/22361 [00:02<00:00, 8371.54 rows/s]
   [info] loading vmp.VIRTUAL_PRODUCT_INGREDIENT: 100% 25080/25080 [00:01<00:00, 16024.16 rows/s]
   [info] loading vmp.ONT_DRUG_FORM: 100% 20133/20133 [00:00<00:00, 22675.82 rows/s]
   [info] loading vmp.DRUG_FORM: 100% 19008/19008 [00:00<00:00, 22321.01 rows/s]
   [info] loading vmp.DRUG_ROUTE: 100% 20646/20646 [00:00<00:00, 26072.49 rows/s]
   [info] loading vmp.CONTROL_DRUG_INFO: 100% 22361/22361 [00:01<00:00, 20707.81 rows/s]
   [info] loading vmpp.VMPPS: 100% 33814/33814 [00:02<00:00, 14685.34 rows/s]
   [info] loading vmpp.DRUG_TARIFF_INFO: 100% 9399/9399 [00:00<00:00, 19506.23 rows/s]
   [info] loading vmpp.COMB_CONTENT: 100% 936/936 [00:00<00:00, 22762.35 rows/s]
   [info] loading amp.AMPS: 100% 147408/147408 [00:14<00:00, 10443.62 rows/s]
   [info] loading amp.AP_INGREDIENT: 100% 14245/14245 [00:00<00:00, 21912.96 rows/s]
   [info] loading amp.LICENSED_ROUTE: 100% 46074/46074 [00:02<00:00, 22430.57 rows/s]
   [info] loading amp.AP_INFORMATION: 100% 80959/80959 [00:03<00:00, 22699.58 rows/s]
   [info] loading ampp.AMPPS: 100% 164827/164827 [00:14<00:00, 11727.62 rows/s]
   [info] loading ampp.APPLIANCE_PACK_INFO: 100% 92238/92238 [00:04<00:00, 21611.34 rows/s]
   [info] loading ampp.DRUG_PRODUCT_PRESCRIB_INFO: 100% 102640/102640 [00:04<00:00, 23009.77 rows/s]
   [info] loading ampp.MEDICINAL_PRODUCT_PRICE: 100% 164827/164827 [00:11<00:00, 14950.48 rows/s]
   [info] loading ampp.REIMBURSEMENT_INFO: 100% 164046/164046 [00:08<00:00, 19182.37 rows/s]
   [info] loading ampp.COMB_CONTENT: 100% 1151/1151 [00:00<00:00, 21601.55 rows/s]
   [info] loading bnf.VMPS: 100% 14980/14980 [00:00<00:00, 22184.29 rows/s]
   [info] loading bnf.AMPS: 100% 5986/5986 [00:00<00:00, 27162.58 rows/s]
   [info] *** END ***


.. _dmd_wh:

``dmd-warehouse-build``
-----------------------

.. argparse::
   :module: dmd.warehouse.warehouse
   :func: _init_cmd_args
   :prog: dmd-warehouse-build

Example
~~~~~~~

To build an SQLite database (``dmd_warehoue.db``) from the input DM+D SQLite databases in a list file ``dbs.list``, run this:

.. code-block:: console

   $ dmd-warehouse-build -v list dbs.list /data/dmd/warehouse/dmd_warehouse.db

The list file will has the structure:

.. code-block::

   /data/dmd/dmd_dbs/2014/nhsbsa_dmd_1.0.0_20131230000001.db
   /data/dmd/dmd_dbs/2014/nhsbsa_dmd_1.1.0_20140106000001.db
   /data/dmd/dmd_dbs/2014/nhsbsa_dmd_1.2.0_20140113000001.db
   /data/dmd/dmd_dbs/2014/nhsbsa_dmd_1.3.0_20140120000001.db
   /data/dmd/dmd_dbs/2014/nhsbsa_dmd_1.4.0_20140127000001.db
   ... more databases here
   /data/dmd/dmd_dbs/2022/nhsbsa_dmd_1.4.0_20220131000001.db
   /data/dmd/dmd_dbs/2022/nhsbsa_dmd_2.0.0_20220207000001.db
   /data/dmd/dmd_dbs/2022/nhsbsa_dmd_2.1.0_20220214000001.db
   /data/dmd/dmd_dbs/2022/nhsbsa_dmd_2.2.0_20220221000001.db
   /data/dmd/dmd_dbs/2022/nhsbsa_dmd_2.3.0_20220228000001.db

Please note that in theory the list file can contain SQLAlchemy connection strings as well (if not using SQLite) or section headings in a config file. However, this has not been tested at all, so may well break. Although SQLite is much more convenient for this sort of thing. Also, there is nothing stopping you using SQLite for individual DM+D version and placing the warehouse in another database back end but this also has not been tested.

The output from the build command should be similar to this:

.. code-block:: console

   = dmd-warehouse (dmd v0.1.0a1) =
   [info] config value: ~/.db_conn.cnf
   [info] create value: False
   [info] is_config value: dmd
   [info] list_file value: /data/dmd/dmd_dbs/dbs.list
   [info] option value: list
   [info] verbose value: True
   [info] whurl value: /data/dmd/warehouse/dmd_warehouse.db
   [info] testing databases: 100% 394/394 [00:01<00:00, 226.42 files/s]
   [info] need to add 2 databases
   [info] adding nhsbsa_dmd_2.2.0_20220221000001 (8087 days)
   [info] testing VMPs...: 172372 VMPs [00:06, 27147.95 VMPs/s]
   [info] added 5 VMP IDs
   [info] testing VMP names...: 172372 VMP names [00:14, 12206.90 VMP names/s]
   [info] updated 6 VMP names
   [info] testing VMP unit dose...: 172372 VMP unit dose [00:09, 18266.58 VMP unit dose/s]
   [info] updated 0 VMP unit dose
   [info] testing VMP ingredients...: 75330 VMP ingredients [00:49, 1518.20 VMP ingredients/s]
   [info] updated 0 VMP ingredients
   [info] testing AMPs...: 172392 AMPs [00:10, 15758.82 AMPs/s]
   [info] added 20 AMP IDs
   [info] testing AMP names...: 172392 AMP names [00:23, 7188.40 AMP names/s] 
   [info] updated 30 AMP names
   [info] testing AMP unit dose...: 172392 AMP unit dose [00:28, 5946.26 AMP unit dose/s] 
   [info] updated 0 AMP unit dose
   [info] testing AMP ingredients...: 167165 AMP ingredients [01:02, 2681.33 AMP ingredients/s] 
   [info] updated 0 AMP ingredients
   [info] adding nhsbsa_dmd_2.3.0_20220228000001 (8094 days)
   [info] testing VMPs...: 172397 VMPs [00:06, 27111.45 VMPs/s]
   [info] added 5 VMP IDs
   [info] testing VMP names...: 172397 VMP names [00:13, 12543.93 VMP names/s]
   [info] updated 6 VMP names
   [info] testing VMP unit dose...: 172397 VMP unit dose [00:10, 17199.57 VMP unit dose/s]
   [info] updated 1 VMP unit dose
   [info] testing VMP ingredients...: 75354 VMP ingredients [00:49, 1521.31 VMP ingredients/s]
   [info] updated 1 VMP ingredients
   [info] testing AMPs...: 172435 AMPs [00:11, 15440.14 AMPs/s]
   [info] added 38 AMP IDs
   [info] testing AMP names...: 172435 AMP names [00:20, 8335.00 AMP names/s] 
   [info] updated 11 AMP names
   [info] testing AMP unit dose...: 172435 AMP unit dose [00:25, 6784.42 AMP unit dose/s] 
   [info] updated 1 AMP unit dose
   [info] testing AMP ingredients...: 167208 AMP ingredients [00:54, 3058.97 AMP ingredients/s] 
   [info] updated 5 AMP ingredients
   [info] *** END ***
