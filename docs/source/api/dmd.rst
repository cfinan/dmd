``dmd`` package
===============

``dmd.build`` module
--------------------

.. autofunction:: dmd.build.build_dmd

.. include:: ../data_dict/dmd-orm.rst
