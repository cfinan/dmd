``dmd.warehouse`` sub-package
=============================

``dmd.warehouse.warehouse`` module
----------------------------------

.. autofunction:: dmd.warehouse.warehouse.add_database

.. include:: ../data_dict/dmd-wh-orm.rst
