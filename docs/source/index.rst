.. dmd documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DMD database
============

dmd is a package to build the NHS DM+D XML distribution files into a relational database. This is for research use only. Please do not use in healthcare implementations.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api
   xml_to_db

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
