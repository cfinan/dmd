# Getting Started with dmd

__version__: `0.2.0a0`

The dmd package is a toolkit to import the NHS Drug and Medical Devices dictionary (DM+D) into a relational database. It is implemented using Python and SQLAlchemy so can be used to build against multiple database backends. However, currently it has only been tested against SQLite and MySQL/MariaDB. How other backends the ORM classes might need some modifications, please let me know if that is the case. More information on DM+D can be found on the NHS digital [TRUD](https://isd.digital.nhs.uk/trud/user/guest/group/0/home) site. You will need an account to subscribe to the resource and download.

There is [online](https://cfinan.gitlab.io/dmd/index.html) documentation for dmd and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/dmd/-/blob/main/resources/pdf/dmd.pdf).

## Installation instructions
At present, dmd is undergoing development and no packages exist yet on PyPy. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
# Change in your package - this is not in git
git clone git@gitlab.com:cfinan/dmd.git
cd dmd
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install dmd as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the dmd repository (or drop `-e` for static install):
```
python -m pip install -e .
```

### Installation using conda
A conda build is also available for Python v3.8 on linux-64/osX-64.
```
conda install -c cfin dmd
```

If you are using conda and require anything different then please install with pip and install the dependencies with the environments specified in `resources/conda_env`. There is also a build recipe in `./resourses/build/conda` that can be used to create new packages.

## Usage TL;DR
Whilst primarily designed for use on the command line there is a stable API endpoint you can use if you want to incorporate into other Python code.

### Command line
To view the help:

```
$ dmd-build --help
usage: build-dmd [-h] [-c CONFIG] [-B BONUS_FILES] [-s CONFIG_SECTION] [-T TMP] [-v] [-e] indata [dburl]

Build a relational database from the NHS DM+D data that is stored within zipped XML files.

positional arguments:
  indata                The input zip archive downloaded from NHS TRUD, do not unzip
  dburl                 An SQLAlchemy connection URL or filename if using SQLite. If you do not want
                        to put full connection parameters on the cmd-line use the config file to
                        supply the parameters

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        The location of the config file
  -B BONUS_FILES, --bonus-files BONUS_FILES
                        The location of the DM+D bonus files
  -s CONFIG_SECTION, --config-section CONFIG_SECTION
                        The section name in the config file
  -T TMP, --tmp TMP     The location of tmp, if not provided will use the system tmp
  -v, --verbose         give more output
  -e, --exists          The database already exists, this will assume that the user has pre-created
                        a database and will not throw errors if the DB/tables already exist
```

To build the DM+D database download the zip archive from the [TRUD](https://isd.digital.nhs.uk/trud/user/guest/group/0/home) site `nhsbsa_dmd_11.4.0_20211129000001.zip` and optionally the supplementary file (bonus file) `nhsbsa_dmdbonus_11.4.0_20211129000001.zip`.

The file names might differ from these ones but will have the structure:

```
nhsbsa_dmd_<major version>.<minor version>.<patch>_YYYYMMDD<some other digits>.zip
nhsbsa_dmdbonus_<major version>.<minor version>.<patch>_YYYYMMDD<some other digits>.zip
```

To load into a SQLite called `~/dmd.db`:

```
$ dmd-build -v -B nhsbsa_dmdbonus_11.4.0_20211129000001.zip nhsbsa_dmd_11.4.0_20211129000001.zip ~/dmd.db
= dmd-build (dmd v0.1.0a1) =
[info] bonus_files value: /data/dmd/nhsbsa_dmdbonus_11.4.0_20211129000001.zip
[info] config value: /home/rmjdcfi/.db_conn.cnf
[info] config_section value: dmd
[info] dburl value: /home/rmjdcfi/dmd.db
[info] exists value: False
[info] indata value: /data/dmd/nhsbsa_dmd_11.4.0_20211129000001.zip
[info] tmp value: None
[info] verbose value: True
[info] loading lookup.COMBINATION_PACK_IND: 100% 2/2 [00:00<00:00, 8756.38 rows/s]
[info] loading lookup.COMBINATION_PROD_IND: 100% 2/2 [00:00<00:00, 11081.38 rows/s]
[info] loading lookup.BASIS_OF_NAME: 100% 7/7 [00:00<00:00, 16230.03 rows/s]
[info] loading lookup.NAMECHANGE_REASON: 100% 5/5 [00:00<00:00, 15603.81 rows/s]
[info] loading lookup.VIRTUAL_PRODUCT_PRES_STATUS: 100% 6/6 [00:00<00:00, 13046.05 rows/s]
[info] loading lookup.CONTROL_DRUG_CATEGORY: 100% 11/11 [00:00<00:00, 17463.04 rows/s]
[info] loading lookup.LICENSING_AUTHORITY: 100% 5/5 [00:00<00:00, 14947.63 rows/s]
[info] loading lookup.UNIT_OF_MEASURE: 100% 182/182 [00:00<00:00, 11919.92 rows/s]
[info] loading lookup.FORM: 100% 192/192 [00:00<00:00, 15348.82 rows/s]
[info] loading lookup.ONT_FORM_ROUTE: 100% 341/341 [00:00<00:00, 17839.87 rows/s]
[info] loading lookup.ROUTE: 100% 71/71 [00:00<00:00, 17194.73 rows/s]
[info] loading lookup.DT_PAYMENT_CATEGORY: 100% 11/11 [00:00<00:00, 16053.36 rows/s]
[info] loading lookup.SUPPLIER: 100% 2158/2158 [00:00<00:00, 20149.12 rows/s]
[info] loading lookup.FLAVOUR: 100% 236/236 [00:00<00:00, 21581.94 rows/s]
[info] loading lookup.COLOUR: 100% 147/147 [00:00<00:00, 20513.11 rows/s]
[info] loading lookup.BASIS_OF_STRNTH: 100% 2/2 [00:00<00:00, 11444.21 rows/s]
[info] loading lookup.REIMBURSEMENT_STATUS: 100% 2/2 [00:00<00:00, 10951.19 rows/s]
[info] loading lookup.SPEC_CONT: 100% 2/2 [00:00<00:00, 11413.07 rows/s]
[info] loading lookup.DND: 100% 2/2 [00:00<00:00, 12264.05 rows/s]
[info] loading lookup.VIRTUAL_PRODUCT_NON_AVAIL: 100% 2/2 [00:00<00:00, 11351.30 rows/s]
[info] loading lookup.DISCONTINUED_IND: 100% 2/2 [00:00<00:00, 10407.70 rows/s]
[info] loading lookup.DF_INDICATOR: 100% 3/3 [00:00<00:00, 14012.15 rows/s]
[info] loading lookup.PRICE_BASIS: 100% 4/4 [00:00<00:00, 6921.29 rows/s]
[info] loading lookup.LEGAL_CATEGORY: 100% 4/4 [00:00<00:00, 14691.08 rows/s]
[info] loading lookup.AVAILABILITY_RESTRICTION: 100% 8/8 [00:00<00:00, 17567.77 rows/s]
[info] loading lookup.LICENSING_AUTHORITY_CHANGE_REASON: 100% 8/8 [00:00<00:00, 16972.40 rows/s]
[info] loading ingredient.INGREDIENT_SUBSTANCES: 100% 3675/3675 [00:00<00:00, 17070.01rows/s]
[info] loading vtm.VIRTUAL_THERAPEUTIC_MOIETIES: 100% 3017/3017 [00:00<00:00, 16172.84rows/s]
[info] loading vmp.VMPS: 100% 22266/22266 [00:02<00:00, 7612.36 rows/s]
[info] loading vmp.VIRTUAL_PRODUCT_INGREDIENT: 100% 24972/24972 [00:01<00:00, 16832.30 rows/s]
[info] loading vmp.ONT_DRUG_FORM: 100% 20034/20034 [00:00<00:00, 24520.19 rows/s]
[info] loading vmp.DRUG_FORM: 100% 18927/18927 [00:00<00:00, 27776.41 rows/s]
[info] loading vmp.DRUG_ROUTE: 100% 20545/20545 [00:00<00:00, 24263.21 rows/s]
[info] loading vmp.CONTROL_DRUG_INFO: 100% 22266/22266 [00:00<00:00, 22716.03 rows/s]
[info] loading vmpp.VMPPS: 100% 33645/33645 [00:02<00:00, 15035.17 rows/s]
[info] loading vmpp.DRUG_TARIFF_INFO: 100% 9364/9364 [00:00<00:00, 20050.38 rows/s]
[info] loading vmpp.COMB_CONTENT: 100% 926/926 [00:00<00:00, 23822.21 rows/s]
[info] loading amp.AMPS: 100% 146526/146526 [00:14<00:00, 10024.80 rows/s]
[info] loading amp.AP_INGREDIENT: 100% 14101/14101 [00:00<00:00, 25237.50 rows/s]
[info] loading amp.LICENSED_ROUTE: 100% 45643/45643 [00:01<00:00, 25994.26 rows/s]
[info] loading amp.AP_INFORMATION: 100% 80511/80511 [00:03<00:00, 22814.95 rows/s]
[info] loading ampp.AMPPS: 100% 163785/163785 [00:13<00:00, 11707.03 rows/s]
[info] loading ampp.APPLIANCE_PACK_INFO: 100% 91711/91711 [00:04<00:00, 19949.66 rows/s]
[info] loading ampp.DRUG_PRODUCT_PRESCRIB_INFO: 100% 102197/102197 [00:04<00:00, 22950.51 rows/s]
[info] loading ampp.MEDICINAL_PRODUCT_PRICE: 100% 163785/163785 [00:11<00:00, 14796.41 rows/s]
[info] loading ampp.REIMBURSEMENT_INFO: 100% 163012/163012 [00:08<00:00, 20100.96 rows/s]
[info] loading ampp.COMB_CONTENT: 100% 1137/1137 [00:00<00:00, 24215.85 rows/s]
[info] loading bnf.VMPS: 100% 14868/14868 [00:00<00:00, 22406.71 rows/s]
[info] loading bnf.AMPS: 100% 5970/5970 [00:00<00:00, 27644.80 rows/s]
[info] *** END ***
```

If you are importing into a database that requires a password then you can supply a configuration file and a section header in the file. An example of a configuration file for MySQL/MariaDB import is below.

```
# The section header can be what you want
[dmd_mysql]
# The dmd. prefix must be there
# passwords must be URL escaped
# the config file must only be readable by you
dmd.url = mysql+pymysql://user:password@127.0.0.1:3306/dmd
```

To use this config file (called `~/.db.cnf`) on the command line:

```
build-dmd -v -c ~/.db.cnf -s "dmd_mysql" -B /data/dmd/nhsbsa_dmdbonus_11.4.0_20211129000001.zip /data/dmd/nhsbsa_dmd_11.4.0_20211129000001.zip
```

### API
There is really only a single function that you need to worry about `build_dmd`.

```python
import sqlalchemy
import dmd

engine = sqlalchemy.create_engine('sqlite:///~/dmd.db')
sm = sqlalchemy.orm.sessionmaker(bind=engine)

zip_file = 'nhsbsa_dmd_11.4.0_20211129000001.zip'
bonus_zip = 'nhsbsa_dmdbonus_11.4.0_20211129000001.zip'

# use system tmp, zips will be unzipped into dirs in there
tmpdir = None
build_dmd(sm, zip_file, tmpdir=None, verbose=False, bonus_zip=bonus_zip)
```

## Other options
I have found one other package for importing DM+D into a MySQL database [here](https://github.com/cbeuw/dmd/blob/master/parser.py). This has provided some inspiration for the code used in this package.

## Schema
The final database will have the following schema (including bonus file).

<div>
<img style="float: right;" width="100%" height="100%" src="./_static/images/dmd_schema.png">
</div>

