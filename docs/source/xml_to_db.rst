=====================================
DM+D XML/relational database mappings
=====================================

The mappings between the XML file and component elements and the relational database tables, ORM classes are shown below.

.. list-table:: XML elements to DB columns
   :widths: 15 15 15 15 15 15 15 15 15 30
   :header-rows: 1

   * - XML file
     - XML element
     - XML sub-element
     - ORM class
     - DB table
     - DB column
     - Data type
     - is nullable
     - is unique
     - Description
   * - ``LOOKUP``
     - ``COMBINATION_PACK_IND``
     - ``CD``
     - ``LkpCombPackInd``
     - ``lkp_comb_pack_ind``
     - ``comb_pack_ind_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``COMBINATION_PACK_IND``
     - ``DESC``
     - ``LkpCombPackInd``
     - ``lkp_comb_pack_ind``
     - ``comb_pack_ind_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The combination pack description.
   * - ``LOOKUP``
     - ``COMBINATION_PROD_IND``
     - ``CD``
     - ``LkpCombProdInd``
     - ``lkp_comb_prod_ind``
     - ``comb_prod_ind_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``COMBINATION_PROD_IND``
     - ``DESC``
     - ``LkpCombProdInd``
     - ``lkp_comb_prod_ind``
     - ``comb_prod_ind_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The combination product description.
   * - ``LOOKUP``
     - ``BASIS_OF_NAME``
     - ``CD``
     - ``LkpBasisOfName``
     - ``lkp_basis_of_name``
     - ``basis_name_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``BASIS_OF_NAME``
     - ``DESC``
     - ``LkpBasisOfName``
     - ``lkp_basis_of_name``
     - ``basis_name_desc``
     - ``VARCHAR(150)``
     - False
     - False
     - The basis of name description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_name_change_reason``
     - ``name_change_reason_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``NAMECHANGE_REASON``
     - ``CD``
     - ``LkpNameChangeReason``
     - ``lkp_name_change_reason``
     - ``name_change_reason_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``NAMECHANGE_REASON``
     - ``DESC``
     - ``LkpNameChangeReason``
     - ``lkp_name_change_reason``
     - ``name_change_reason_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The name change reason description.
   * - ``LOOKUP``
     - ``VIRTUAL_PRODUCT_PRES_STATUS``
     - ``CD``
     - ``LkpVirtualProdPresStatus``
     - ``lkp_virtual_prod_pres_status``
     - ``pres_status_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``VIRTUAL_PRODUCT_PRES_STATUS``
     - ``DESC``
     - ``LkpVirtualProdPresStatus``
     - ``lkp_virtual_prod_pres_status``
     - ``pres_status_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The virtual medicinal product prescribing status description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_control_drug_cat``
     - ``cont_drug_cat_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``CONTROL_DRUG_CATEGORY``
     - ``CD``
     - ``LkpControlDrugCat``
     - ``lkp_control_drug_cat``
     - ``cont_drug_cat_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``CONTROL_DRUG_CATEGORY``
     - ``DESC``
     - ``LkpControlDrugCat``
     - ``lkp_control_drug_cat``
     - ``cont_drug_cat_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The controlled drug category description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_licence_auth``
     - ``lic_auth_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``LICENSING_AUTHORITY``
     - ``CD``
     - ``LkpLicenceAuth``
     - ``lkp_licence_auth``
     - ``lic_auth_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``LICENSING_AUTHORITY``
     - ``DESC``
     - ``LkpLicenceAuth``
     - ``lkp_licence_auth``
     - ``lic_auth_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The licensing authority description.
   * - ``LOOKUP``
     - ``UNIT_OF_MEASURE``
     - ``CD``
     - ``LkpUnitMeasure``
     - ``lkp_unit_measure``
     - ``unit_meas_cd``
     - ``BIGINT``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file. Apparently it is a SNOMED code up to 18 digits long.
   * - ``LOOKUP``
     - ``UNIT_OF_MEASURE``
     - ``CDDT``
     - ``LkpUnitMeasure``
     - ``lkp_unit_measure``
     - ``unit_meas_cd_date``
     - ``DATE``
     - True
     - False
     - The date the code is applicable from.
   * - ``LOOKUP``
     - ``UNIT_OF_MEASURE``
     - ``CDPREV``
     - ``LkpUnitMeasure``
     - ``lkp_unit_measure``
     - ``unit_meas_cd_prev``
     - ``BIGINT``
     - True
     - False
     - Previous code, up to 18 digits.
   * - ``LOOKUP``
     - ``UNIT_OF_MEASURE``
     - ``DESC``
     - ``LkpUnitMeasure``
     - ``lkp_unit_measure``
     - ``unit_meas_desc``
     - ``VARCHAR(255)``
     - False
     - False
     - The unit of measure description.
   * - ``LOOKUP``
     - ``FORM``
     - ``CD``
     - ``LkpForm``
     - ``lkp_form``
     - ``form_cd``
     - ``BIGINT``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file. This is a SNOMED code up to a maximum of 18 digits.
   * - ``LOOKUP``
     - ``FORM``
     - ``CDDT``
     - ``LkpForm``
     - ``lkp_form``
     - ``form_cd_date``
     - ``DATE``
     - True
     - False
     - Date the code is applicable from.
   * - ``LOOKUP``
     - ``FORM``
     - ``CDPREV``
     - ``LkpForm``
     - ``lkp_form``
     - ``form_cd_prev``
     - ``BIGINT``
     - True
     - False
     - Previous form code, upto 18 digits.
   * - ``LOOKUP``
     - ``FORM``
     - ``DESC``
     - ``LkpForm``
     - ``lkp_form``
     - ``form_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The form code description.
   * - ``LOOKUP``
     - ``ONT_FORM_ROUTE``
     - ``CD``
     - ``LkpOntFormRoute``
     - ``lkp_ont_form_route``
     - ``ont_form_route_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``ONT_FORM_ROUTE``
     - ``DESC``
     - ``LkpOntFormRoute``
     - ``lkp_ont_form_route``
     - ``ont_form_route_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The ontology form route description.
   * - ``LOOKUP``
     - ``ROUTE``
     - ``CD``
     - ``LkpRoute``
     - ``lkp_route``
     - ``route_cd``
     - ``BIGINT``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file. This is a SNOMED code up to a maximum of 18 digits.
   * - ``LOOKUP``
     - ``ROUTE``
     - ``CDDT``
     - ``LkpRoute``
     - ``lkp_route``
     - ``route_cd_date``
     - ``DATE``
     - True
     - False
     - The date the code is applicable from.
   * - ``LOOKUP``
     - ``ROUTE``
     - ``CDPREV``
     - ``LkpRoute``
     - ``lkp_route``
     - ``route_cd_prev``
     - ``BIGINT``
     - True
     - False
     - The previous route of administration code (max 18 digits).
   * - ``LOOKUP``
     - ``ROUTE``
     - ``DESC``
     - ``LkpRoute``
     - ``lkp_route``
     - ``route_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The route of administration description.
   * - ``LOOKUP``
     - ``DT_PAYMENT_CATEGORY``
     - ``CD``
     - ``LkpDrugTariffCat``
     - ``lkp_drug_tariff_cat``
     - ``drug_tariff_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``DT_PAYMENT_CATEGORY``
     - ``DESC``
     - ``LkpDrugTariffCat``
     - ``lkp_drug_tariff_cat``
     - ``drug_tariff_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The drug tariff pay category description.
   * - ``LOOKUP``
     - ``SUPPLIER``
     - ``CD``
     - ``LkpSupplier``
     - ``lkp_supplier``
     - ``supplier_cd``
     - ``BIGINT``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file. This is a SNOMED code up to a maximum of 18 digits.
   * - ``LOOKUP``
     - ``SUPPLIER``
     - ``CDDT``
     - ``LkpSupplier``
     - ``lkp_supplier``
     - ``supplier_cd_date``
     - ``DATE``
     - True
     - False
     - The date the code is applicable from.
   * - ``LOOKUP``
     - ``SUPPLIER``
     - ``CDPREV``
     - ``LkpSupplier``
     - ``lkp_supplier``
     - ``supplier_cd_prev``
     - ``BIGINT``
     - True
     - False
     - Previous code, max of 18 digits.
   * - ``LOOKUP``
     - ``SUPPLIER``
     - ``INVALID``
     - ``LkpSupplier``
     - ``lkp_supplier``
     - ``supplier_invalid``
     - ``SMALLINT``
     - True
     - False
     - Is the code invalid?
   * - ``LOOKUP``
     - ``SUPPLIER``
     - ``DESC``
     - ``LkpSupplier``
     - ``lkp_supplier``
     - ``supplier_desc``
     - ``VARCHAR(80)``
     - False
     - False
     - The supplier description.
   * - ``LOOKUP``
     - ``FLAVOUR``
     - ``CD``
     - ``LkpFlavour``
     - ``lkp_flavour``
     - ``flav_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``FLAVOUR``
     - ``DESC``
     - ``LkpFlavour``
     - ``lkp_flavour``
     - ``flav_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The flavour description.
   * - ``LOOKUP``
     - ``COLOUR``
     - ``CD``
     - ``LkpColour``
     - ``lkp_colour``
     - ``colour_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``COLOUR``
     - ``DESC``
     - ``LkpColour``
     - ``lkp_colour``
     - ``colour_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The colour description.
   * - ``LOOKUP``
     - ``BASIS_OF_STRNTH``
     - ``CD``
     - ``LkpBasisStrength``
     - ``lkp_basis_strength``
     - ``basis_strength_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``BASIS_OF_STRNTH``
     - ``DESC``
     - ``LkpBasisStrength``
     - ``lkp_basis_strength``
     - ``basis_strength_desc``
     - ``VARCHAR(150)``
     - False
     - False
     - The basis of strength description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_reimb_status``
     - ``reimb_status_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``REIMBURSEMENT_STATUS``
     - ``CD``
     - ``LkpReimbStatus``
     - ``lkp_reimb_status``
     - ``reimb_status_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``REIMBURSEMENT_STATUS``
     - ``DESC``
     - ``LkpReimbStatus``
     - ``lkp_reimb_status``
     - ``reimb_status_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The reimbursement status description.
   * - ``LOOKUP``
     - ``SPEC_CONT``
     - ``CD``
     - ``LkpSpecialCont``
     - ``lkp_special_cont``
     - ``special_cont_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``SPEC_CONT``
     - ``DESC``
     - ``LkpSpecialCont``
     - ``lkp_special_cont``
     - ``special_cont_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The special container description.
   * - ``LOOKUP``
     - ``DND``
     - ``CD``
     - ``LkpDnd``
     - ``lkp_dnd``
     - ``dnd_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``DND``
     - ``DESC``
     - ``LkpDnd``
     - ``lkp_dnd``
     - ``dnd_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The discount not deducted description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_virt_prod_non_avail``
     - ``virt_med_prod_na_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``VIRTUAL_PRODUCT_NON_AVAIL``
     - ``CD``
     - ``LkpVirtMedProdNa``
     - ``lkp_virt_prod_non_avail``
     - ``virt_med_prod_na_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``VIRTUAL_PRODUCT_NON_AVAIL``
     - ``DESC``
     - ``LkpVirtMedProdNa``
     - ``lkp_virt_prod_non_avail``
     - ``virt_med_prod_na_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The VMP non availability description.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``lkp_discon_ind``
     - ``discon_ind_id``
     - ``INTEGER``
     - False
     - True
     - The primary key, this is added by the build script and not the original primary key in the XML file. This is due to the original key starting at 0 and causing issues with MySQL auto-increment and SQLAchemy. For the original key see the ``_cd`` suffixed column.
   * - ``LOOKUP``
     - ``DISCONTINUED_IND``
     - ``CD``
     - ``LkpDisconInd``
     - ``lkp_discon_ind``
     - ``discon_ind_cd``
     - ``INTEGER``
     - False
     - True
     - A unique key, this column represents the primary key in the original XML file however because it incremented from 0 a surrogate primary key was created in the ``_id`` suffixed column.
   * - ``LOOKUP``
     - ``DISCONTINUED_IND``
     - ``DESC``
     - ``LkpDisconInd``
     - ``lkp_discon_ind``
     - ``discon_ind_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The discontinued indicator description.
   * - ``LOOKUP``
     - ``DF_INDICATOR``
     - ``CD``
     - ``LkpDoseFormInd``
     - ``lkp_dose_form_ind``
     - ``dose_form_ind_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``DF_INDICATOR``
     - ``DESC``
     - ``LkpDoseFormInd``
     - ``lkp_dose_form_ind``
     - ``dose_form_ind_desc``
     - ``VARCHAR(20)``
     - False
     - False
     - The dose form indicator description.
   * - ``LOOKUP``
     - ``PRICE_BASIS``
     - ``CD``
     - ``LkpBasisPrice``
     - ``lkp_basis_price``
     - ``basis_price_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``PRICE_BASIS``
     - ``DESC``
     - ``LkpBasisPrice``
     - ``lkp_basis_price``
     - ``basis_price_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The basis of price description.
   * - ``LOOKUP``
     - ``LEGAL_CATEGORY``
     - ``CD``
     - ``LkpLegalCat``
     - ``lkp_legal_cat``
     - ``legal_cat_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``LEGAL_CATEGORY``
     - ``DESC``
     - ``LkpLegalCat``
     - ``lkp_legal_cat``
     - ``legal_cat_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The legal category description.
   * - ``LOOKUP``
     - ``AVAILABILITY_RESTRICTION``
     - ``CD``
     - ``LkpAvailRestict``
     - ``lkp_avail_restrict``
     - ``avail_restrict_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``AVAILABILITY_RESTRICTION``
     - ``DESC``
     - ``LkpAvailRestict``
     - ``lkp_avail_restrict``
     - ``avail_restrict_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The availability restriction description.
   * - ``LOOKUP``
     - ``LICENSING_AUTHORITY_CHANGE_REASON``
     - ``CD``
     - ``LkpLicAuthChange``
     - ``lkp_lic_auth_change``
     - ``lic_auth_change_cd``
     - ``INTEGER``
     - False
     - True
     - The primary key, whilst this is set as an auto-increment but is sourced from the XML file.
   * - ``LOOKUP``
     - ``LICENSING_AUTHORITY_CHANGE_REASON``
     - ``DESC``
     - ``LkpLicAuthChange``
     - ``lkp_lic_auth_change``
     - ``lic_auth_change_desc``
     - ``VARCHAR(60)``
     - False
     - False
     - The licensing authority reason for change description.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``VTMID``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_id``
     - ``BIGINT``
     - False
     - True
     - Virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``INVALID``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_invalid``
     - ``SMALLINT``
     - True
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``NM``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_name``
     - ``VARCHAR(255)``
     - True
     - False
     - Virtual therapeutic moiety name.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``ABBREVNM``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_abbrev``
     - ``VARCHAR(60)``
     - True
     - False
     - Virtual therapeutic moiety abbreviated name.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``VTMIDPREV``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_id_prev``
     - ``BIGINT``
     - True
     - False
     - Previous virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_THERAPEUTIC_MOIETIES``
     - ``VTM``
     - ``VTMIDDT``
     - ``VirtTherMoiet``
     - ``virt_ther_moiet``
     - ``vtm_id_date``
     - ``DATE``
     - True
     - False
     - The date the virtual therapeutic moiety identifier became valid.
   * - ``INGREDIENT_SUBSTANCES``
     - ``ING``
     - ``ISID``
     - ``Ingredient``
     - ``ingredient``
     - ``ingredient_id``
     - ``BIGINT``
     - False
     - True
     - Ingredient substance identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``INGREDIENT_SUBSTANCES``
     - ``ING``
     - ``ISIDDT``
     - ``Ingredient``
     - ``ingredient``
     - ``ingredient_id_date``
     - ``DATE``
     - True
     - False
     - The date the ingredient substance identifier became valid.
   * - ``INGREDIENT_SUBSTANCES``
     - ``ING``
     - ``ISIDPREV``
     - ``Ingredient``
     - ``ingredient``
     - ``ingredient_id_prev``
     - ``BIGINT``
     - True
     - False
     - Previous ingredient substance identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``INGREDIENT_SUBSTANCES``
     - ``ING``
     - ``INVALID``
     - ``Ingredient``
     - ``ingredient``
     - ``ingredient_invalid``
     - ``SMALLINT``
     - True
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``INGREDIENT_SUBSTANCES``
     - ``ING``
     - ``NM``
     - ``Ingredient``
     - ``ingredient``
     - ``ingredient_name``
     - ``VARCHAR(255)``
     - True
     - False
     - The ingredient substance name
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``VPID``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - True
     - Virtual medical product identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``VPIDDT``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_id_date``
     - ``DATE``
     - True
     - False
     - The date when the virtual medicinal product identifier became valid.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``VPIDPREV``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_id_prev``
     - ``BIGINT``
     - True
     - False
     - Previous VMP identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``VTMID``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vtm_id``
     - ``BIGINT``
     - True
     - False
     - Virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``INVALID``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_invalid``
     - ``SMALLINT``
     - False
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NM``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_name``
     - ``VARCHAR(255)``
     - False
     - False
     - The virtual medical product name.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``ABBREVNM``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_name_abbrev``
     - ``VARCHAR(60)``
     - True
     - False
     - The abbreviated virtual medical product name.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NMDT``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_name_date``
     - ``DATE``
     - True
     - False
     - The date the VMP name became valid.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NMPREV``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``vmp_name_prev``
     - ``VARCHAR(255)``
     - True
     - False
     - The previous virtual medical product name.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``BASISCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``basis_name_cd``
     - ``INTEGER``
     - False
     - False
     - The basis of name code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``BASIS_PREVCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``basis_name_cd_prev``
     - ``INTEGER``
     - True
     - False
     - The previous basis of name code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NMCHANGECD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``name_change_reason_cd``
     - ``INTEGER``
     - True
     - False
     - The reason for name change code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``COMBPRODCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``comb_prod_ind_cd``
     - ``INTEGER``
     - True
     - False
     - The combination product indicator code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``PRES_STATCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``pres_status_cd``
     - ``INTEGER``
     - False
     - False
     - The VMP prescribing status.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``SUG_F``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``sugar_free``
     - ``SMALLINT``
     - True
     - False
     - The sugar free indicator.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``GLU_F``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``gluten_free``
     - ``SMALLINT``
     - True
     - False
     - The gluten free indicator.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``PRES_F``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``pres_free``
     - ``SMALLINT``
     - True
     - False
     - The preservative free indicator.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``CFC_F``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``cfc_free``
     - ``SMALLINT``
     - True
     - False
     - The CFC free indicator.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NON_AVAILCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``virt_med_prod_na_cd``
     - ``INTEGER``
     - True
     - False
     - The non-availability indicator code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``NON_AVAILDT``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``virt_med_prod_na_date``
     - ``DATE``
     - True
     - False
     - The non-availability status date.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``DF_INDCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``dose_form_ind_cd``
     - ``INTEGER``
     - True
     - False
     - The dose form indicator code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``UDFS``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``unit_dose_form_size``
     - ``FLOAT``
     - True
     - False
     - Unit dose form size - a numerical value relating to size of entity. This will only be present if the unit dose form attribute is discrete. Up to a maximum of 10 digits and 3 decimal places.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``UDFS_UOMCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - Unit dose form units - unit of measure code relating to the size. This will only be present if the unit dose form attribute is discrete. Narrative can be located in lookup file under tag <UNIT_OF_MEASURE> Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VMPS``
     - ``UNIT_DOSE_UOMCD``
     - ``VirtMedProd``
     - ``virt_med_prod``
     - ``unit_meas_ud_cd``
     - ``BIGINT``
     - True
     - False
     - Unit dose unit of measure - unit of measure code relating to a description of the entity that can be handled. This will only be present if the Unit dose form attribute is discrete. Narrative can be located in lookup file under tag <UNIT_OF_MEASURE>.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``virt_prod_ing``
     - ``vpi_id``
     - ``INTEGER``
     - False
     - True
     - The virtual product ingredient ID, this is the primary key and is NOT derived from the XML file. I think it is required as there is a many-to-one relationship between ingredients and products (although not 100% sure).
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``VPID``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - The virtual medicinal product ID. This is derived from the XML file and is a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``ISID``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``vmp_ingredient_id``
     - ``BIGINT``
     - False
     - False
     - Ingredient substance identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``BASIS_STRNTCD``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``basis_strength_cd``
     - ``INTEGER``
     - True
     - False
     - The basis of pharmaceutical strength code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``BS_SUBID``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``basis_strength_sub_id``
     - ``BIGINT``
     - True
     - False
     - Virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``STRNT_NMRTR_VAL``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``stren_value_num``
     - ``FLOAT``
     - True
     - False
     - Strength value numerator - value of numerator element of strength up to a maximum of 10 digits and 3 decimal places.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``STRNT_NMRTR_UOMCD``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``stren_value_num_unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - Strength value numerator unit - numerator value of strength unit of measure code. Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``STRNT_DNMTR_VAL``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``stren_value_den``
     - ``FLOAT``
     - True
     - False
     - Strength value denominator - value of denominator element of strength. Up to a maximum of 10 digits and 3 decimal places.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``VIRTUAL_PRODUCT_INGREDIENT``
     - ``STRNT_DNMTR_UOMCD``
     - ``VirtProdIng``
     - ``virt_prod_ing``
     - ``stren_value_den_unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - Strength value denominator unit denominator value of strength unit of measure code. Up to a maximum of 18 digits.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``ont_drug_form``
     - ``odf_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``ONT_DRUG_FORM``
     - ``VPID``
     - ``OntDrugForm``
     - ``ont_drug_form``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``ONT_DRUG_FORM``
     - ``FORMCD``
     - ``OntDrugForm``
     - ``ont_drug_form``
     - ``ont_form_route_cd``
     - ``INTEGER``
     - False
     - False
     - Virtual medicinal product form & route code. Up to 4 digits (the XML file says always 4 characters but the XML schema is integer!?).
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``drug_form``
     - ``df_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``DRUG_FORM``
     - ``VPID``
     - ``DrugForm``
     - ``drug_form``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``DRUG_FORM``
     - ``FORMCD``
     - ``DrugForm``
     - ``drug_form``
     - ``form_cd``
     - ``BIGINT``
     - False
     - False
     - Formulation code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``drug_route``
     - ``dr_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``DRUG_ROUTE``
     - ``VPID``
     - ``DrugRoute``
     - ``drug_route``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``DRUG_ROUTE``
     - ``ROUTECD``
     - ``DrugRoute``
     - ``drug_route``
     - ``route_cd``
     - ``BIGINT``
     - False
     - False
     - Route (of administration?) code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``cont_drug_info``
     - ``cdi_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``CONTROL_DRUG_INFO``
     - ``VPID``
     - ``ContDrugInfo``
     - ``cont_drug_info``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``CONTROL_DRUG_INFO``
     - ``CATCD``
     - ``ContDrugInfo``
     - ``cont_drug_info``
     - ``cont_drug_cat_cd``
     - ``INTEGER``
     - False
     - False
     - The controlled drug category code.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``CONTROL_DRUG_INFO``
     - ``CATDT``
     - ``ContDrugInfo``
     - ``cont_drug_info``
     - ``cont_drug_date``
     - ``DATE``
     - True
     - False
     - Controlled drug applicable date.
   * - ``VIRTUAL_MED_PRODUCTS``
     - ``CONTROL_DRUG_INFO``
     - ``CAT_PREVCD``
     - ``ContDrugInfo``
     - ``cont_drug_info``
     - ``cont_drug_cat_cd_prev``
     - ``INTEGER``
     - True
     - False
     - Previous control drug information. Controlled drug category prior to change date.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``VPPID``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``vmp_pack_id``
     - ``BIGINT``
     - False
     - True
     - Virtual medicinal product pack identifier (SNOMED Code). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``INVALID``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``vmp_pack_invalid``
     - ``INTEGER``
     - True
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``NM``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``vmp_pack_name``
     - ``VARCHAR(420)``
     - False
     - False
     - The virtual medicinal product pack name.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``ABBREVNM``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``vmp_pack_name_abrv``
     - ``VARCHAR(255)``
     - True
     - False
     - The abbreviated virtual medicinal product pack name. Not in the XML documentation so not sure on correct field size.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``VPID``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``QTYVAL``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``quant_val``
     - ``FLOAT``
     - True
     - False
     - Quantity value up to a maximum of 10 digits and 2 decimal places.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``QTY_UOMCD``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``quant_unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - Quantity value unit of measure code. Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``VMPP``
     - ``COMBPACKCD``
     - ``VirtMedProdPack``
     - ``virt_med_prod_pack``
     - ``comb_pack_ind_cd``
     - ``INTEGER``
     - True
     - False
     - Combination pack indicator code. Up to 4 digits.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``drug_tariff_info``
     - ``dti_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``DRUG_TARIFF_INFO``
     - ``VPPID``
     - ``DrugTariffInfo``
     - ``drug_tariff_info``
     - ``vmp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product pack identifier (SNOMED Code). Up to a maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``DRUG_TARIFF_INFO``
     - ``PAY_CATCD``
     - ``DrugTariffInfo``
     - ``drug_tariff_info``
     - ``drug_tariff_cd``
     - ``INTEGER``
     - False
     - False
     - Drug tariff payment category code.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``DRUG_TARIFF_INFO``
     - ``PRICE``
     - ``DrugTariffInfo``
     - ``drug_tariff_info``
     - ``drug_tariff_price``
     - ``INTEGER``
     - True
     - False
     - Drug tariff price.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``DRUG_TARIFF_INFO``
     - ``DT``
     - ``DrugTariffInfo``
     - ``drug_tariff_info``
     - ``drug_tariff_date``
     - ``DATE``
     - True
     - False
     - The date that the drug tariff price is valid from.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``DRUG_TARIFF_INFO``
     - ``PREVPRICE``
     - ``DrugTariffInfo``
     - ``drug_tariff_info``
     - ``drug_tariff_price_prev``
     - ``INTEGER``
     - True
     - False
     - The drug tariff price before the ``drug_tariff_date``.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``vmp_pack_comb_content``
     - ``vmp_pack_comb_pack_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``COMB_CONTENT``
     - ``PRNTVPPID``
     - ``VmppCombContent``
     - ``vmp_pack_comb_content``
     - ``vmp_pack_parent_id``
     - ``BIGINT``
     - False
     - False
     - The parent virtual medicinal product pack identifier (SNOMED Code). Up to maximum of 18 digits.
   * - ``VIRTUAL_MED_PRODUCT_PACK``
     - ``COMB_CONTENT``
     - ``CHLDVPPID``
     - ``VmppCombContent``
     - ``vmp_pack_comb_content``
     - ``vmp_pack_child_id``
     - ``BIGINT``
     - False
     - False
     - The child virtual medicinal product pack identifier (SNOMED Code). Up to maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``APID``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_id``
     - ``BIGINT``
     - False
     - True
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``INVALID``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_invalid``
     - ``SMALLINT``
     - True
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``VPID``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``NM``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_name``
     - ``VARCHAR(255)``
     - False
     - False
     - Actual medicinal product name.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``ABBREVNM``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_name_abbrev``
     - ``VARCHAR(60)``
     - True
     - False
     - Abbreviated actual medicinal product name.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``DESC``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_desc``
     - ``VARCHAR(700)``
     - False
     - False
     - Actual medicinal product description.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``NMDT``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_name_date``
     - ``DATE``
     - True
     - False
     - Date when the actual medicinal product name became applicable.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``NM_PREV``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``amp_name_prev``
     - ``VARCHAR(255)``
     - True
     - False
     - Previous actual medicinal product name.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``SUPPCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``supplier_cd``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product supplied code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``LIC_AUTHCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``lic_auth_cd``
     - ``INTEGER``
     - False
     - False
     - Actual medicinal product licensing authority code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``LIC_AUTH_PREVCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``lic_auth_cd_prev``
     - ``INTEGER``
     - True
     - False
     - Previous actual medicinal product licensing authority code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``LIC_AUTHCHANGECD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``lic_auth_change_cd``
     - ``INTEGER``
     - True
     - False
     - Code for the reason for change of licensing authority.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``LIC_AUTHCHANGEDT``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``lic_auth_change_date``
     - ``DATE``
     - True
     - False
     - Date of change of licensing authority.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``COMBPRODCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``comb_prod_ind_cd``
     - ``INTEGER``
     - True
     - False
     - Combination product indicator code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``FLAVOURCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``flav_cd``
     - ``INTEGER``
     - True
     - False
     - Flavour code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``EMA``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``ema``
     - ``SMALLINT``
     - True
     - False
     - EMA additional monitoring indicator.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``PARALLEL_IMPORT``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``parallel_imp``
     - ``SMALLINT``
     - True
     - False
     - Parallel import indicator.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AMPS``
     - ``AVAIL_RESTRICTCD``
     - ``ActMedProd``
     - ``act_med_prod``
     - ``avail_restrict_cd``
     - ``INTEGER``
     - False
     - False
     - Restrictions on availability code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``act_prod_ing``
     - ``api_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INGREDIENT``
     - ``APID``
     - ``ActProdIng``
     - ``act_prod_ing``
     - ``amp_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INGREDIENT``
     - ``ISID``
     - ``ActProdIng``
     - ``act_prod_ing``
     - ``amp_ingredient_id``
     - ``BIGINT``
     - False
     - False
     - Ingredient substance identifier (SNOMED Code). Up to a maximum of 18 integers.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INGREDIENT``
     - ``STRNTH``
     - ``ActProdIng``
     - ``act_prod_ing``
     - ``strength``
     - ``FLOAT``
     - True
     - False
     - Pharmaceutical strength numerical value - strength value. Up to a maximum of 10 digits and 3 decimal places.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INGREDIENT``
     - ``UOMCD``
     - ``ActProdIng``
     - ``act_prod_ing``
     - ``unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - Pharmaceutical strength unit measure code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``licenced_route``
     - ``lr_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``LICENSED_ROUTE``
     - ``APID``
     - ``LicencedRoute``
     - ``licenced_route``
     - ``amp_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``LICENSED_ROUTE``
     - ``ROUTECD``
     - ``LicencedRoute``
     - ``licenced_route``
     - ``lr_rt_cd``
     - ``BIGINT``
     - False
     - False
     - Licensed route code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``app_prod_info``
     - ``app_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INFORMATION``
     - ``APID``
     - ``AppProdInfo``
     - ``app_prod_info``
     - ``amp_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INFORMATION``
     - ``SZ_WEIGHT``
     - ``AppProdInfo``
     - ``app_prod_info``
     - ``size_weight``
     - ``VARCHAR(100)``
     - True
     - False
     - Size/weight.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INFORMATION``
     - ``COLOURCD``
     - ``AppProdInfo``
     - ``app_prod_info``
     - ``colour_cd``
     - ``INTEGER``
     - True
     - False
     - The colour code.
   * - ``ACTUAL_MEDICINAL_PRODUCTS``
     - ``AP_INFORMATION``
     - ``PROD_ORDER_NO``
     - ``AppProdInfo``
     - ``app_prod_info``
     - ``prod_order_no``
     - ``VARCHAR(20)``
     - True
     - False
     - The product order number.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``APPID``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``amp_pack_id``
     - ``BIGINT``
     - False
     - True
     - Actual medicinal product pack identifier (SNOMED Code), up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``INVALID``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``amp_pack_invalid``
     - ``SMALLINT``
     - True
     - False
     - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``NM``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``amp_pack_name``
     - ``VARCHAR(774)``
     - False
     - False
     - Actual medicinal product pack description.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``ABBREVNM``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``amp_pack_name_abrv``
     - ``VARCHAR(60)``
     - True
     - False
     - Abbreviated actual medicinal product pack description.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``VPPID``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``vmp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product pack identifier (SNOMED Code). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``APID``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``amp_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``COMBPACKCD``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``comb_pack_ind_cd``
     - ``INTEGER``
     - True
     - False
     - Combination pack indicator code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``LEGAL_CATCD``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``legal_cat_cd``
     - ``INTEGER``
     - False
     - False
     - Legal category code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``SUBP``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``sub_pack_info``
     - ``VARCHAR(30)``
     - True
     - False
     - Sub-pack information.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``DISCCD``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``discon_ind_cd``
     - ``INTEGER``
     - True
     - False
     - Discontinued flag code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``AMPPS``
     - ``DISCDT``
     - ``ActMedProdPack``
     - ``act_med_prod_pack``
     - ``discon_ind_date``
     - ``DATE``
     - True
     - False
     - Discontinued flag change date.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``app_pack_info``
     - ``app_pack_info_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``APPLIANCE_PACK_INFO``
     - ``APPID``
     - ``AppPackInfo``
     - ``app_pack_info``
     - ``amp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product pack identifier (SNOMED Code), up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``APPLIANCE_PACK_INFO``
     - ``REIMB_STATCD``
     - ``AppPackInfo``
     - ``app_pack_info``
     - ``reimb_status_cd``
     - ``INTEGER``
     - False
     - False
     - Appliance reimbursement status code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``APPLIANCE_PACK_INFO``
     - ``REIMB_STATDT``
     - ``AppPackInfo``
     - ``app_pack_info``
     - ``reimb_status_date``
     - ``DATE``
     - True
     - False
     - Date appliance reimbursement status code becomes effective.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``APPLIANCE_PACK_INFO``
     - ``REIMB_STATPREVCD``
     - ``AppPackInfo``
     - ``app_pack_info``
     - ``reimb_status_cd_prev``
     - ``INTEGER``
     - True
     - False
     - Previous appliance reimbursement status code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``APPLIANCE_PACK_INFO``
     - ``PACK_ORDER_NO``
     - ``AppPackInfo``
     - ``app_pack_info``
     - ``pack_order_no``
     - ``VARCHAR(20)``
     - True
     - False
     - Pack order number - order number of pack within drug tariff. Up to a maximum of 20 characters.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``drug_pres_info``
     - ``dpi_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``APPID``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``amp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product pack identifier (SNOMED Code), up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``SCHED_2``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``schedule_two``
     - ``SMALLINT``
     - True
     - False
     - Schedule two indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``ACBS``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``acbs``
     - ``SMALLINT``
     - True
     - False
     - ACBS indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``PADM``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``personal_admin``
     - ``SMALLINT``
     - True
     - False
     - Personally administered indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``FP10_MDA``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``fp_ten``
     - ``SMALLINT``
     - True
     - False
     - FP10 MDA prescription indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``SCHED_1``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``schedule_one``
     - ``SMALLINT``
     - True
     - False
     - Schedule one indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``HOSP``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``hospital``
     - ``SMALLINT``
     - True
     - False
     - Hospital indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``NURSE_F``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``nurse``
     - ``SMALLINT``
     - True
     - False
     - Nurse formulary indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``ENURSE_F``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``nurse_ext``
     - ``SMALLINT``
     - True
     - False
     - Nurse extended formulary indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``DRUG_PRODUCT_PRESCRIB_INFO``
     - ``DENT_F``
     - ``DrugPresInfo``
     - ``drug_pres_info``
     - ``dental``
     - ``SMALLINT``
     - True
     - False
     - Dental formulary indicator.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``med_prod_price``
     - ``mpp_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``MEDICINAL_PRODUCT_PRICE``
     - ``APPID``
     - ``MedProdPrice``
     - ``med_prod_price``
     - ``amp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product pack identifier (SNOMED Code), up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``MEDICINAL_PRODUCT_PRICE``
     - ``PRICE``
     - ``MedProdPrice``
     - ``med_prod_price``
     - ``price``
     - ``INTEGER``
     - True
     - False
     - Actual medicinal product pack price.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``MEDICINAL_PRODUCT_PRICE``
     - ``PRICEDT``
     - ``MedProdPrice``
     - ``med_prod_price``
     - ``price_date``
     - ``DATE``
     - True
     - False
     - Date of price validity.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``MEDICINAL_PRODUCT_PRICE``
     - ``PRICE_PREV``
     - ``MedProdPrice``
     - ``med_prod_price``
     - ``price_prev``
     - ``INTEGER``
     - True
     - False
     - Previous actual medicinal product pack price.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``MEDICINAL_PRODUCT_PRICE``
     - ``PRICE_BASISCD``
     - ``MedProdPrice``
     - ``med_prod_price``
     - ``basis_price_cd``
     - ``INTEGER``
     - False
     - False
     - Basis of price code.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``reimb_info``
     - ``ri_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``APPID``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``amp_pack_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product pack identifier (SNOMED Code), up to a maximum of 18 digits.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``PX_CHRGS``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``pres_charges``
     - ``INTEGER``
     - True
     - False
     - Prescription charges.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``DISP_FEES``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``disp_fees``
     - ``INTEGER``
     - True
     - False
     - Dispensing fees.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``BB``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``broken_bulk``
     - ``SMALLINT``
     - True
     - False
     - Broken bulk indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``LTD_STAB``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``ltd_stab``
     - ``INTEGER``
     - True
     - False
     - The Drug Tariff no longer identifies products for this purpose. Therefore this indicator is no longer populated in dm+d. The data field will persist but remains blank.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``CAL_PACK``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``calendar_pack``
     - ``SMALLINT``
     - True
     - False
     - Calendar pack indicator.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``SPEC_CONTCD``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``special_cont_cd``
     - ``INTEGER``
     - True
     - False
     - Special container indicator code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``DND``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``dnd_cd``
     - ``INTEGER``
     - True
     - False
     - Discount not deducted indicator code.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``REIMBURSEMENT_INFO``
     - ``FP34D``
     - ``ReimbInfo``
     - ``reimb_info``
     - ``fp_three_four_d``
     - ``SMALLINT``
     - True
     - False
     - FP34D prescription item indicator.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``ampp_comb_pack_content``
     - ``ampp_comb_pack_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``COMB_CONTENT``
     - ``PRNTAPPID``
     - ``AmppCombPackContent``
     - ``ampp_comb_pack_content``
     - ``amp_pack_parent_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product combination pack content parent ID.
   * - ``ACTUAL_MEDICINAL_PROD_PACKS``
     - ``COMB_CONTENT``
     - ``CHLDAPPID``
     - ``AmppCombPackContent``
     - ``ampp_comb_pack_content``
     - ``amp_pack_child_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product combination pack content child ID.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``virt_med_prod_map``
     - ``vmpm_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``BNF_DETAILS``
     - ``VMPS``
     - ``VPID``
     - ``VirtMedProdMap``
     - ``virt_med_prod_map``
     - ``vmp_id``
     - ``BIGINT``
     - False
     - False
     - Virtual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``BNF_DETAILS``
     - ``VMPS``
     - ``BNF``
     - ``VirtMedProdMap``
     - ``virt_med_prod_map``
     - ``bnf``
     - ``INTEGER``
     - True
     - False
     - The British National Formulary (BNF) chapter mapping.
   * - ``BNF_DETAILS``
     - ``VMPS``
     - ``ATC``
     - ``VirtMedProdMap``
     - ``virt_med_prod_map``
     - ``atc``
     - ``VARCHAR(255)``
     - True
     - False
     - The Anatomic Therapeutic Chemical (ATC) code mapping.
   * - ``BNF_DETAILS``
     - ``VMPS``
     - ``DDD``
     - ``VirtMedProdMap``
     - ``virt_med_prod_map``
     - ``ddd``
     - ``FLOAT``
     - True
     - False
     - The defined daily dose.
   * - ``BNF_DETAILS``
     - ``VMPS``
     - ``DDD_UOMCD``
     - ``VirtMedProdMap``
     - ``virt_med_prod_map``
     - ``ddd_unit_meas_cd``
     - ``BIGINT``
     - True
     - False
     - The drug daily dose unit of measure.
   * - .
     - .
     - .
     - ``DeclarativeMeta``
     - ``act_med_prod_map``
     - ``ampm_id``
     - ``INTEGER``
     - False
     - True
     - The primary key - this is not part of the XML file.
   * - ``BNF_DETAILS``
     - ``AMPS``
     - ``APID``
     - ``ActMedProdMap``
     - ``act_med_prod_map``
     - ``amp_id``
     - ``BIGINT``
     - False
     - False
     - Actual medicinal product identifier (as above in VMP tag). Up to a maximum of 18 digits.
   * - ``BNF_DETAILS``
     - ``AMPS``
     - ``BNF``
     - ``ActMedProdMap``
     - ``act_med_prod_map``
     - ``bnf``
     - ``INTEGER``
     - True
     - False
     - The British National Formulary (BNF) chapter mapping.
