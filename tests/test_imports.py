import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'dmd.common',
        'dmd.orm',
        'dmd.query',
        'dmd.build',
        'dmd.warehouse.warehouse',
        'dmd.warehouse.join',
        'dmd.warehouse.orm',
        'dmd.warehouse.query',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    module = importlib.import_module(import_loc, package=None)

